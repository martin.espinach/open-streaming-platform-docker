#!/usr/bin/env bash
echo 'Placing Configuration Files'
cp -R -u -p /opt/osp/setup/nginx/*.conf /usr/local/nginx/conf/
cp -u -p /opt/osp/setup/nginx/mime.types /usr/local/nginx/conf/
echo 'Setting up Directories'
if [ ! -d /var/www ]; then
  mkdir -p /var/www && \
  mkdir -p /var/www/live && \
  mkdir -p /var/www/videos && \
  mkdir -p /var/www/live-rec && \
  mkdir -p /var/www/live-adapt && \
  mkdir -p /var/www/stream-thumb && \
  mkdir -p /var/www/images  && \
  mkdir -p /var/log/gunicorn && \
  chown -R www-data:www-data /var/www && \
  chown -R www-data:www-data /var/log/gunicorn
fi
  
echo 'Setting up OSP Configuration'
if [ ! -f /opt/osp/conf/config.py ]; then
  export DB_URL
  echo "dbLocation='$DB_URL'" > /opt/osp/conf/config.py
  export REDIS_HOST
  echo "redisHost='$REDIS_HOST'" >> /opt/osp/conf/config.py
  export REDIS_PORT
  echo "redisPort=$REDIS_PORT" >> /opt/osp/conf/config.py
  export REDIS_PASSWORD
  echo "redisPassword='$REDIS_PASSWORD'" >> /opt/osp/conf/config.py
  export FLASK_SECRET
  echo "secretKey='$FLASK_SECRET'" >> /opt/osp/conf/config.py
  export FLASK_SALT
  echo "passwordSalt='$FLASK_SALT'" >> /opt/osp/conf/config.py
  export OSP_ALLOWREGISTRATION
  echo "allowRegistration=$OSP_ALLOWREGISTRATION" >> /opt/osp/conf/config.py
  export OSP_REQUIREVERIFICATION
  echo "requireEmailRegistration=$OSP_REQUIREVERIFICATION" >> /opt/osp/conf/config.py
  echo "debugMode=False" >> /opt/osp/conf/config.py
  
  echo '# EJabberD Configuration' >> /opt/osp/conf/config.py
  echo 'ejabberdAdmin = "admin"' >> /opt/osp/conf/config.py
  echo 'ejabberdPass = "CHANGE_EJABBERD_PASS"' >> /opt/osp/conf/config.py
  echo 'ejabberdHost = "localhost"' >> /opt/osp/conf/config.py
  EJABBERD_ADMINPASS=$( cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1 )
  sed -i "s/CHANGE_EJABBERD_PASS/$EJABBERD_ADMINPASS/" /opt/osp/conf/config.py
  
  echo 'Setting up ejabberd'
  export OSP_SERVER_ADDRESS
  sed -i "s/CHANGEME/$OSP_SERVER_ADDRESS/g" /usr/local/ejabberd/etc/ejabberd/ejabberd.yml
fi

echo 'Starting ejabberd'
/usr/local/ejabberd/sbin/ejabberdctl start
/usr/local/ejabberd/sbin/ejabberdctl started
/usr/local/ejabberd/sbin/ejabberdctl register admin localhost $EJABBERD_ADMINPASS

chown -R www-data:www-data /opt/osp/conf/config.py
echo 'Performing DB Migrations'
cd /opt/osp

if [[ ! -d /opt/osp/migrations ]]; then
    python3 manage.py db init
fi
python3 manage.py db migrate
python3 manage.py db upgrade
cd /

echo 'Fixing OSP Permissions Post Migration'
chown -R www-data:www-data /opt/osp

echo 'Attempting initial provisioning by environment variables (if needed)'
python3 /opt/osp/setup/env_initial_setup.py &

echo 'Starting OSP'
supervisord --nodaemon --configuration /opt/osp/setup/supervisord.conf